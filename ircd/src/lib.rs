#[derive(Debug, PartialEq)]
pub enum OmegaRequest {
    Upgrade,
    Shutdown,
}

impl std::fmt::Display for OmegaRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let p = match *self {
            OmegaRequest::Upgrade => "Upgrade",
            OmegaRequest::Shutdown => "Shutdown",
        };

        write!(f, "{}", p)
    }
}
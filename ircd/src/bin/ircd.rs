extern crate ircd;
extern crate libloading;

use ircd::OmegaRequest;
use libloading::{Library, Result, Symbol};
use std::env;

type OmegaFunc  = fn() -> OmegaRequest;

fn main() {
    let library_path = env::args().nth(1).expect("Usage: load <LIB>");
    println!("Loading library from: {:?}", library_path);

    loop {
        let lib = Library::new(&library_path).expect("Unable to load library");

        let request = run(&lib);

        println!("Dropping {:?}", lib);

        drop(lib);

        if request == OmegaRequest::Shutdown {
            break;
        }
    }
}

fn run(lib: &Library) -> OmegaRequest {
    unsafe {
        let func: Symbol<OmegaFunc> = lib.get(b"omega_main").expect("Symbol not found");
        let request = func();

        println!("Quit request: {}", request);

        request
    }
}
use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub struct IrcMessage {
    pub source: Option<String>,
    pub command: String,
    pub args: Vec<String>,
}

#[derive(Debug, PartialEq)]
pub struct ParseIrcMessageError;

impl FromStr for IrcMessage {
    type Err = ParseIrcMessageError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // Reverse so we can use pop
        let mut parts: Vec<&str> = s.split(' ').rev().collect();

        if parts.len() < 2 {
            return Err(ParseIrcMessageError);
        }

        let mut source = None;
        let head = parts.pop().unwrap();
        let command;

        if head.starts_with(':') {
            source = Some(head[1..].to_string());
            command = parts.pop().unwrap().to_string();
        } else {
            command = head.to_string();
        }

        let mut args = vec![];

        while let Some(s) = parts.pop() {
            if s.starts_with(':') {
                // Push back without : and then join.
                parts.push(&s[1..]);
                break;
            }

            args.push(s.to_string());
        }

        // Reverse again so we can properly join
        parts.reverse();

        args.push(parts.join(" "));

        Ok(IrcMessage {
            source,
            command,
            args,
        })
    }
}

impl ToString for IrcMessage {
    fn to_string(&self) -> String {
        let mut res: String = String::from("");

        if let Some(source) = &self.source {
            res += &format!(":{} ", source);
        }

        res += &format!("{} ", &self.command);

        if self.args.len() > 0 {
            let last_index = self.args.len() - 1;

            res += &self.args[..last_index].join(" ");

            res += &format!(" :{}", self.args[last_index]);
        }

        res
    }
}

#[test]
fn irc_message_from_str_test_single() {
    let string = ":server.irc COMMAND param :params";

    let result = IrcMessage::from_str(string).ok().unwrap();

    assert_eq!(result.source, Some("server.irc".to_string()));
    assert_eq!(result.command, "COMMAND".to_string());
    assert_eq!(result.args, vec!["param", "params"]);

    assert_eq!(string, result.to_string());
}

#[test]
fn irc_message_from_str_test_multi_param() {
    let string = ":server.irc COMMAND param :param1 param2";

    let result = IrcMessage::from_str(string).ok().unwrap();

    assert_eq!(result.source, Some("server.irc".to_string()));
    assert_eq!(result.command, "COMMAND".to_string());
    assert_eq!(result.args, vec!["param", "param1 param2"]);

    assert_eq!(string, result.to_string());
}

#[test]
fn irc_message_from_str_test_no_source() {
    let string = "COMMAND param :param1 param2";

    let result = IrcMessage::from_str(string).ok().unwrap();

    assert_eq!(result.source, None);
    assert_eq!(result.command, "COMMAND".to_string());
    assert_eq!(result.args, vec!["param", "param1 param2"]);

    assert_eq!(string, result.to_string());
}

#[test]
fn irc_message_from_str_test_err() {
    let string = "COMMAND";
    let result = IrcMessage::from_str(string);

    assert_eq!(Err(ParseIrcMessageError), result);
}

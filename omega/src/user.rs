use self::Mode::*;
use std::net::IpAddr;

#[derive(PartialEq, Eq, Hash)]
pub struct User {
    nickname_ts: u64,
    connection_ts: u64,
    nickname: String,
    ident: String,
    host_info: HostInfo,
    ip_info: IpInfo,
    modes: Vec<Mode>,
    uid: String,
    certfp: Option<String>,
    gecos: String,
}

#[derive(PartialEq, Eq, Hash)]
struct HostInfo {
    normal: String,
    cloaked: Option<String>,
    cgi: Option<String>,
    visible: String,
}

#[derive(PartialEq, Eq, Hash)]
struct IpInfo {
    normal: Option<IpAddr>,
    cgi: Option<IpAddr>,
    cloaked: Option<String>,
}

#[derive(PartialEq, Eq, Hash, Debug)]
pub enum Mode {
    IRCOperator,          // = 'O',
    Invisible,            // = 'i',
    Wallops,              // = 'w',
    WebIRC,               // = 'W',
    Operwall,             // = 'z',
    Locops,               // = 'l',
    Cloaked,              // = 'x',
    Spy,                  // = 'y',
    Registered,           // = 'r',
    IgnoreRegistered,     // = 'R',
    ServerKills,          // = 's',
    SSL,                  // = 'S',
    ServerAdmin,          // = 'a',
    BlockCTCP,            // = 'C',
    Deaf,                 // = 'D',
    SoftCallerId,         // = 'g',
    CallerId,             // = 'G',
    Private,              // = 'p',
    NetworkService,       // = 'U',
    NetworkAdministrator, // = 'N',
}

impl Mode {
    fn try_from(input: char) -> Option<Self> {
        match input {
            'O' => Some(IRCOperator),
            'i' => Some(Invisible),
            'w' => Some(Wallops),
            'W' => Some(WebIRC),
            'z' => Some(Operwall),
            'l' => Some(Locops),
            'x' => Some(Cloaked),
            'y' => Some(Spy),
            'r' => Some(Registered),
            'R' => Some(IgnoreRegistered),
            's' => Some(ServerKills),
            'S' => Some(SSL),
            'a' => Some(ServerAdmin),
            'C' => Some(BlockCTCP),
            'D' => Some(Deaf),
            'g' => Some(SoftCallerId),
            'G' => Some(CallerId),
            'p' => Some(Private),
            'U' => Some(NetworkService),
            'N' => Some(NetworkAdministrator),
            _ => None,
        }
    }
}

impl Into<char> for Mode {
    fn into(self) -> char {
        'a'
    }
}

#[test]
fn try_from_test() {
    assert_eq!(Mode::try_from('O'), Some(IRCOperator));
    assert_eq!(Mode::try_from('i'), Some(Invisible));
    assert_eq!(Mode::try_from('w'), Some(Wallops));
    assert_eq!(Mode::try_from('W'), Some(WebIRC));
    assert_eq!(Mode::try_from('z'), Some(Operwall));
    assert_eq!(Mode::try_from('l'), Some(Locops));
    assert_eq!(Mode::try_from('x'), Some(Cloaked));
    assert_eq!(Mode::try_from('y'), Some(Spy));
    assert_eq!(Mode::try_from('r'), Some(Registered));
    assert_eq!(Mode::try_from('R'), Some(IgnoreRegistered));
    assert_eq!(Mode::try_from('s'), Some(ServerKills));
    assert_eq!(Mode::try_from('S'), Some(SSL));
    assert_eq!(Mode::try_from('a'), Some(ServerAdmin));
    assert_eq!(Mode::try_from('C'), Some(BlockCTCP));
    assert_eq!(Mode::try_from('D'), Some(Deaf));
    assert_eq!(Mode::try_from('g'), Some(SoftCallerId));
    assert_eq!(Mode::try_from('G'), Some(CallerId));
    assert_eq!(Mode::try_from('p'), Some(Private));
    assert_eq!(Mode::try_from('U'), Some(NetworkService));
    assert_eq!(Mode::try_from('N'), Some(NetworkAdministrator));
}

// [06-27 21:46] umode          +o     - Designates this client is an IRC Operator.
// [06-27 21:46] umode                   Use the /oper command to attain this.
// [06-27 21:46] umode          +i     - Designates this client 'invisible'.
// [06-27 21:46] umode          +w     - Can see wallops.
// [06-27 21:46] umode          +W     - User is connected using a webirc gateway.
// [06-27 21:46] umode        * +z     - Can see oper wallops.
// [06-27 21:46] umode        * +l     - Can see oper locops (local wallops).
// [06-27 21:46] umode          +x     - "cloak": this client's hostname has been 'cloaked'.
// [06-27 21:46] umode        * +y     - Can see whois notices.
// [06-27 21:46] umode          +r     - User has been registered and identified for its nick.
// [06-27 21:46] umode                   This mode can be set by servers and services only.
// [06-27 21:46] umode          +R     - Only users logged in to services may message you.
// [06-27 21:46] umode        * +s     - Can see generic server messages and oper kills.
// [06-27 21:46] umode          +S     - Client is connected via SSL/TLS
// [06-27 21:46] umode        * +a     - Is marked as a server admin in stats p/o.
// [06-27 21:46] umode          +C     - Blocks CTCP messages.
// [06-27 21:46] umode          +D     - "deaf": don't receive channel messages
// [06-27 21:46] umode          +G     - "soft caller id": block private messages from people not on
// [06-27 21:46] umode                   any common channels with you (unless they are /accept'ed)
// [06-27 21:46] umode          +g     - "caller id" mode: only allow /accept clients to message you
// [06-27 21:46] umode          +p     - Hides the output of channels this client is in from WHOIS
// [06-27 21:46] umode                   to non-operators.

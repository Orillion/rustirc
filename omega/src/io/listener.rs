use mio::net::{TcpListener, TcpStream};
use mio::Poll;
use mio::PollOpt;
use mio::Ready;
use mio::Token;
use std::io::Result;
use std::net::SocketAddr;
use token::TokenGenerator;

pub fn listen(poll: &Poll, token_generator: &mut TokenGenerator, addr: &SocketAddr) -> Result<(TcpListener, Token)> {
    let server = TcpListener::bind(addr)?;

    let token = token_generator.generate();

    poll.register(&server, token, Ready::readable(), PollOpt::edge());

    Ok((server, token))
}

pub fn accept(poll: &Poll, token_generator: &mut TokenGenerator, listener: &TcpListener) -> Result<(TcpStream, Token)> {
    let (sock, _) = listener.accept()?;

    let token = token_generator.generate();

    poll.register(&sock, token, Ready::readable(), PollOpt::edgs());

    Ok((sock, token))
}

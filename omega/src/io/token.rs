use mio::Token;

pub struct TokenGenerator {
    tokens: Vec<usize>,
    max_token: usize,
}

impl TokenGenerator {
    /// Creates a new TokenGenerator.
    /// ```
    /// let tg = TokenGenerator::new();
    /// ```
    /// 
    pub fn new() -> Self {
        TokenGenerator {
            tokens: vec![0],
            max_token: 0,
        }
    }

    /// Returns a token that's unique for this TokenGenerator.
    /// ```
    /// let tg = TokenGenerator::new();
    /// 
    /// let token = tg.generate();
    /// 
    /// assert_eq!(Token(0), token);
    /// ```
    pub fn generate(&mut self) -> Token {
        if let Some(token) = self.tokens.pop() {
            Token(token)
        } else {
            self.max_token += 1;
            Token(self.max_token)
        }
    }

    pub fn return_token(&mut self, token: Token) {
        self.tokens.push(token.0);
    }
}

#[test]
fn token_generator_test() {
    let mut tg = TokenGenerator::new();

    assert_eq!(Token(0), tg.generate());
    assert_eq!(Token(1), tg.generate());
    assert_eq!(Token(2), tg.generate());
    
    tg.return_token(Token(1));

    assert_eq!(Token(1), tg.generate());
}
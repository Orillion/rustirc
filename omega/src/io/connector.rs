use std::net::SocketAddr;
use mio::net::TcpStream;
use mio::Poll;
use mio::PollOpt;
use mio::Token;
use mio::Ready;
use std::io::Result;
use super::token::TokenGenerator;

pub fn connect(poll: &Poll, token_generator: &mut TokenGenerator, addr: &SocketAddr) -> Result<(TcpStream, Token)> {
    let sock = TcpStream::connect(addr)?;

    let token = token_generator.generate();

    poll.register(&sock, token, Ready::readable() | Ready::writable(), PollOpt::edge())?;

    Ok((sock, token))
}   
use mio::net::TcpStream;
use mio::*;
use std::collections::HashMap;
use std::io::Read;
use std::io::Result;
use std::io::Write;

mod connector;
mod token;

use self::token::TokenGenerator;

pub struct SocketParty {
    pub sock: TcpStream,
    pub write_handler: Option<fn(sock: &mut SocketParty)>,
    pub read_handler: Option<fn(sock: &mut SocketParty, message: &String)>,
    pub buffer: String,
}

impl SocketParty {
    pub fn new(sock: TcpStream) -> Self {
        SocketParty {
            sock,
            write_handler: None,
            read_handler: None,
            buffer: String::with_capacity(512),
        }
    }
}

pub fn connect(poll: &Poll, sockets: &mut HashMap<Token, SocketParty>) -> Result<Token> {
    let addr = "192.168.2.10:27888".parse().unwrap();

    let mut token_generator = TokenGenerator::new();

    let (sock, token) = connector::connect(&poll, &mut token_generator, &addr)?;

    sockets.insert(token, SocketParty::new(sock));

    Ok(token)
}

pub fn io_loop(poll: &Poll, sockets: &mut HashMap<Token, SocketParty>) -> Result<()> {
    let mut events = Events::with_capacity(128);
    poll.poll(&mut events, None)?;

    let mut buffer = [0; 512];

    for event in events {
        let mut s = sockets.get_mut(&event.token()).unwrap();

        if event.readiness().is_readable() {
            while let Ok(bytes) = s.sock.read(&mut buffer) {
                if bytes <= 0 {
                    break;
                }

                if let Ok(string) = String::from_utf8(buffer[0..bytes].to_vec()) {
                    s.buffer += &string;
                } else {
                    // Terminate connection
                    continue;
                }

                while let Some(command) = irc_split(&mut s.buffer) {
                    if let Some(handler) = s.read_handler {
                        handler(s, &command);
                    }
                }
            }
        }

        if event.readiness().is_writable() {
            if let Some(handler) = s.write_handler {
                handler(s);
            }
        }
    }

    Ok(())
}

fn _connect_test() {
    let mut map = HashMap::new();
    let poll = Poll::new().unwrap();

    if let Ok(token) = connect(&poll, &mut map) {
        let mut sock = map.get_mut(&token).unwrap();
        sock.write_handler = Some(|s| {
            s.sock.write(b"PASS test TS 6 :00T\r\n").unwrap();
            s.sock.write(b"CAPAB :TS6 EX IE HUB KLN GLN ENCAP EOB TBURST QS\r\n")
                .unwrap();
            s.sock.write(b"SERVER test.orillion.net 1 :Rizon Test Server\r\n")
                .unwrap();
            s.sock.write(b"SVINFO 6 6 0 :0\r\n").unwrap();
            s.sock.write(b":00T EOB\r\n").unwrap();
            s.sock.flush().unwrap();
            s.write_handler = None;
        });
        sock.read_handler = Some(|s, message| {
            println!("[{}] <- {}", s.sock.peer_addr().unwrap(), message);
        });
    }

    loop {
        io_loop(&poll, &mut map);
    }
}

fn irc_split(s: &mut String) -> Option<String> {
    if let Some(index) = s.find("\r\n") {
        let mut start: String = s.drain(..index + 2).collect();
        start.truncate(index);
        Some(start)
    } else {
        None
    }
}

#[test]
fn split_test() {
    let mut s = "hello\r\ngood bye\r\nhello".to_string();
    let mut v = vec![];

    while let Some(e) = irc_split(&mut s) {
        v.push(e);
    }

    assert_eq!(v, ["hello", "good bye"]);
    assert_eq!(s, "hello");
}

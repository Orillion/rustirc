use self::Mode::*;

#[derive(Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum Mode {
    InviteOnly,           // = 'i'
    SSLOnly,              // = 'S',
    NoControlCodes,       // = 'c',
    NoExternalMessages,   // = 'n',
    OpsTopic,             // = 't',
    Secret,               // = 's',
    Paranoia,             // = 'p',
    Moderated,            // = 'm',
    Persist,              // = 'z',
    BandwidthSaver,       // = 'B',
    NoCTCP,               // = 'C',
    ModReg,               // = 'M',
    NoNotices,            // = 'N',
    IRCOpsOnly,           // = 'O',
    RegisteredOnly,       // = 'R',
    Key(String),          // = 'k',
    Limit(u32),           // = 'l',
    Voice(String),        // = 'v',
    HalfOp(String),       // = 'h',
    Op(String),           // = 'o',
    Admin(String),        // = 'a',
    Owner(String),        // = 'q',
    Ban(String),          // = 'b',
    Exempt(String),       // = 'e',
    InviteExempt(String), // = 'I',
}

#[derive(Debug, Eq, PartialEq)]
pub struct ModeParseError;

macro_rules! mode_with_string_arg {
    ($v:expr, $mode:expr) => {
        if let Some(v) = $v {
            Some($mode(v.to_string()))
        } else {
            return Err(ModeParseError);
        }
    };
}

macro_rules! mode_with_u32_arg {
    ($v:expr, $mode:expr) => {
        if let Some(v) = $v {
            if let Ok(limit) = v.parse::<u32>() {
                Some($mode(limit))
            } else {
                return Err(ModeParseError);
            }
        } else {
            return Err(ModeParseError);
        }
    };
}

impl Mode {
    pub fn from_mode_string(string: &str) -> Result<Vec<Self>, ModeParseError> {
        let mut result = vec![];

        let s = string.to_string();
        let mut args = s.split(' ');

        if let Some(s) = args.next() {
            let mut chars = s.chars();

            while let Some(character) = chars.next() {
                let mode = match character {
                    'i' => Some(InviteOnly),
                    'S' => Some(SSLOnly),
                    'c' => Some(NoControlCodes),
                    'n' => Some(NoExternalMessages),
                    't' => Some(OpsTopic),
                    's' => Some(Secret),
                    'p' => Some(Paranoia),
                    'm' => Some(Moderated),
                    'z' => Some(Persist),
                    'B' => Some(BandwidthSaver),
                    'C' => Some(NoCTCP),
                    'M' => Some(ModReg),
                    'N' => Some(NoNotices),
                    'O' => Some(IRCOpsOnly),
                    'R' => Some(RegisteredOnly),
                    'k' => mode_with_string_arg!(args.next(), Key),
                    'l' => mode_with_u32_arg!(args.next(), Limit),
                    'v' => mode_with_string_arg!(args.next(), Voice),
                    'h' => mode_with_string_arg!(args.next(), HalfOp),
                    'o' => mode_with_string_arg!(args.next(), Op),
                    'a' => mode_with_string_arg!(args.next(), Admin),
                    'q' => mode_with_string_arg!(args.next(), Owner),
                    'b' => mode_with_string_arg!(args.next(), Ban),
                    'e' => mode_with_string_arg!(args.next(), Exempt),
                    'I' => mode_with_string_arg!(args.next(), InviteExempt),
                    _ => None,
                };

                if let Some(mode) = mode {
                    result.push(mode);
                }
            }
        }

        Ok(result)
    }
}

#[test]
fn from_mode_string_test() {
    // Indiviual Mode Tests
    assert_eq!(Ok(vec![InviteOnly]), Mode::from_mode_string("i"));
    assert_eq!(Ok(vec![SSLOnly]), Mode::from_mode_string("S"));
    assert_eq!(Ok(vec![NoControlCodes]), Mode::from_mode_string("c"));
    assert_eq!(Ok(vec![NoExternalMessages]), Mode::from_mode_string("n"));
    assert_eq!(Ok(vec![OpsTopic]), Mode::from_mode_string("t"));
    assert_eq!(Ok(vec![Secret]), Mode::from_mode_string("s"));
    assert_eq!(Ok(vec![Paranoia]), Mode::from_mode_string("p"));
    assert_eq!(Ok(vec![Moderated]), Mode::from_mode_string("m"));
    assert_eq!(Ok(vec![Persist]), Mode::from_mode_string("z"));
    assert_eq!(Ok(vec![BandwidthSaver]), Mode::from_mode_string("B"));
    assert_eq!(Ok(vec![NoCTCP]), Mode::from_mode_string("C"));
    assert_eq!(Ok(vec![ModReg]), Mode::from_mode_string("M"));
    assert_eq!(Ok(vec![NoNotices]), Mode::from_mode_string("N"));
    assert_eq!(Ok(vec![IRCOpsOnly]), Mode::from_mode_string("O"));
    assert_eq!(Ok(vec![RegisteredOnly]), Mode::from_mode_string("R"));
    assert_eq!(
        Ok(vec![Key(String::from("password"))]),
        Mode::from_mode_string("k password")
    );
    assert_eq!(Ok(vec![Limit(42)]), Mode::from_mode_string("l 42"));
    assert_eq!(
        Ok(vec![Voice(String::from("Dwarf"))]),
        Mode::from_mode_string("v Dwarf")
    );
    assert_eq!(
        Ok(vec![HalfOp(String::from("Dwarf"))]),
        Mode::from_mode_string("h Dwarf")
    );
    assert_eq!(
        Ok(vec![Op(String::from("Dwarf"))]),
        Mode::from_mode_string("o Dwarf")
    );
    assert_eq!(
        Ok(vec![Admin(String::from("Dwarf"))]),
        Mode::from_mode_string("a Dwarf")
    );
    assert_eq!(
        Ok(vec![Owner(String::from("Dwarf"))]),
        Mode::from_mode_string("q Dwarf")
    );
    assert_eq!(
        Ok(vec![Ban(String::from("Dwarf!*@*"))]),
        Mode::from_mode_string("b Dwarf!*@*")
    );
    assert_eq!(
        Ok(vec![Exempt(String::from("Dwarf!*@*"))]),
        Mode::from_mode_string("e Dwarf!*@*")
    );
    assert_eq!(
        Ok(vec![InviteExempt(String::from("Dwarf!*@*"))]),
        Mode::from_mode_string("I Dwarf!*@*")
    );
    assert_eq!(
        Ok(vec![InviteOnly, Voice("Dwarf".to_string())]),
        Mode::from_mode_string("iv Dwarf")
    );
    assert_eq!(
        Ok(vec![InviteOnly, IRCOpsOnly, Voice("Dwarf".to_string())]),
        Mode::from_mode_string("iOv Dwarf")
    );
}

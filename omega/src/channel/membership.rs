use std::rc::Rc;

use super::Channel;
use super::Mode;
use super::User;

#[derive(PartialEq, Eq, Hash)]
pub struct Membership {
    user: Rc<User>,
    channel: Rc<Channel>,
    modes: Vec<Mode>,
}

use super::user::User;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::rc::Rc;
use std::string::String;
pub mod membership;
pub mod mode;
use self::membership::Membership;
use self::mode::Mode;

#[derive(PartialEq, Eq)]
pub struct Channel {
    ts: i64,
    name: String,
    members: HashMap<Rc<User>, Membership>,
    modes: Vec<Mode>,
}

impl Hash for Channel {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl Channel {
    pub fn new(ts: i64, name: String) -> Self {
        Channel {
            ts,
            name,
            members: HashMap::new(),
            modes: Vec::new(),
        }
    }
}

#[test]
fn test_channel_new() {
    let channel = Channel::new(1, "Channel".to_string());
    assert_eq!(channel.ts, 1);
    assert_eq!(channel.name, "Channel");
}

extern crate ircd;
extern crate mio;

use ircd::OmegaRequest;
use ircd::OmegaRequest::*;
use mio::Poll;
use std::collections::HashMap;

mod channel;
//pub mod plugin;
mod io;
mod user;
mod encoding;

use encoding::IrcMessage;
use std::str::FromStr;

#[no_mangle]
pub fn omega_main() -> OmegaRequest {
    let mut map = HashMap::new();
    let poll = Poll::new().unwrap();

    if let Ok(token) = io::connect(&poll, &mut map) {
        let mut sock = map.get_mut(&token).unwrap();
        sock.read_handler = Some(|s, message| {

            if let Ok(m) = IrcMessage::from_str(message) {
                println!("[{}] <- {:?}", s.sock.peer_addr().unwrap(), m);
            } else {
                println!("[{}] <- {}", s.sock.peer_addr().unwrap(), message);
            }
        });
    }

    loop {
        if let Err(_) = io::io_loop(&poll, &mut map) {
            break;
        }
    }

    Upgrade
}

// #[derive(Debug, Default)]
// pub struct Omega;

// impl Plugin for Omega {
//     fn name(&self) -> &'static str {
//         "Omega"
//     }

//     fn on_plugin_load(&self) {
//         println!("Starting {} plugin", self.name());
//     }

//     fn on_plugin_unload(&self) {
//         println!("Stopping {} plugin", self.name());
//     }
// }

// declare_plugin!(Omega, Omega::default);
